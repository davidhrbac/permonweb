# Team

<div markdown>
<figure markdown>
![](assets/team/dostal.jpg){: style="width:10rem; border-radius:5rem;border:5px solid var(--md-primary-fg-color);"}
  <figcaption><a href="https://innet.vsb.cz/personCards/personCard.jsp?lang=en&person=DOS35">Zdeněk Dostál</a></figcaption>
</figure>
<figure markdown>
![](assets/team/horak.jpg){: style="width:10rem; border-radius:5rem;border:5px solid var(--md-primary-fg-color);"}
  <figcaption><a href="https://innet.vsb.cz/personCards/personCard.jsp?lang=en&person=HOR##">David Horák</a></figcaption>
</figure>
<figure markdown>
![](assets/team/hapla.jpg){: style="width:10rem; border-radius:5rem;border:5px solid var(--md-primary-fg-color);"}
  <figcaption>Václav Hapla</figcaption>
</figure>
<figure markdown>
![](assets/team/cermak.jpg){: style="width:10rem; border-radius:5rem;border:5px solid var(--md-primary-fg-color);"}
  <figcaption><a href="https://innet.vsb.cz/personCards/personCard.jsp?lang=en&person=CER365">Martin Čermák</a></figcaption>
</figure>
<figure markdown>
![](assets/team/sojka.jpg){: style="width:10rem; border-radius:5rem;border:5px solid var(--md-primary-fg-color);"}
  <figcaption>Radim Sojka</figcaption>
</figure>
<figure markdown>
![](assets/team/pecha.jpg){: style="width:10rem; border-radius:5rem;border:5px solid var(--md-primary-fg-color);"}
  <figcaption><a href="https://innet.vsb.cz/personCards/personCard.jsp?lang=en&person=PEC0031">Marek Pecha</a></figcaption>
</figure>
<figure markdown>
![](assets/team/kruzik.jpg){: style="width:10rem; border-radius:5rem;border:5px solid var(--md-primary-fg-color);"}
  <figcaption><a href="https://innet.vsb.cz/personCards/personCard.jsp?lang=en&person=KRU0097">Jakub Kružík</a></figcaption>
</figure>
</div>

## Contributors

* [Lukáš Pospíšil](https://innet.vsb.cz/personCards/personCard.jsp?lang=en&person=POS220)
* Alena Vašatová
* Alexandros Markopoulos
