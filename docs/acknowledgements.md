# Acknowledgement

We would like to acknowledge the following organizations and projects supporting development of PERMON:

* Department of Applied Mathematics at VŠB - Technical University of Ostrava, Czech Republic for support and teaching whole PERMON team and interesting discusions
* IT4Innovations National Supercomputing Center, VŠB - Technical University of Ostrava, Czech Republic and  Anselm and Salomon clusters, operated by IT4Innovations
* the internal student grant competition project PERMON toolbox development I, II, III  (SP2015/186, SP2016/178, SP2017/169SP2017/169) and  SGS No. SP2018/165 VSB - Technical University of Ostrava, Czech Republic
* the POSTDOCI II project (CZ.1.07/2.3.00/30.0055) within Operational Programme Education for Competitiveness
* the  Czech Science Foundation (GACR) project no. 15-18274S
* the Ministry of Education, Youth and Sports from the National Programme of Sustainability (NPU II) project IT4Innovations excellence in science (LQ1602), and from the Large Infrastructures for Research, Experimental Development and Innovations project IT4Innovations National Supercomputing Center (LM2015070)
* ARCHER, the UK's national high-performance computing service, provided by The Engineering and Physical Sciences Research Council (EPSRC), The Natural Environment Research Council (NERC), EPCC, Cray Inc. and The University of Edinburgh
* the  PRACE 1, 2, 3 IP projects under grant agreements no.: RI-261557, 283493, 312763
* the EXA2CT project funded from the EU's Seventh Framework Programme (FP7/2007-2013) under grant agreement no. 610741
* the READEX project - the European Union’s Horizon 2020 research and innovation programme under grant agreement no. 671657.
* the grant of the Czech Science Foundation (GACR) project no. GA17-22615S
* European Union’s Horizon 2020 research and innovation programme under grant agreement number 847593 and by The Czech Radioactive Waste Repository Authority (SÚRAO) under grant agreement number SO2020-017
* Grant Agency of the Czech Republic, project No. 19-11441S
* SGS SP2021/103 of VSB-Technical University of Ostrava
* RRC/10/2019 Support for Science and Research in the Moravia-Silesia Region 2
* DECI resource Eagle based in Poland at <a href=https://wiki.man.poznan.pl/hpc/index.php?title=Eagle>https://wiki.man.poznan.pl/hpc</a> with support from the PRACE aisbl
* Technology Agency of the Czech Repulic Project No. TK02010118, Prediction of Excavation Damage Zone properties for safety and reliability of a deep geological repository - ENDORSE
