![](assets/permon_full.png){ width=33% align=right }

**PERMON** is a set of solvers combining quadratic programming and domain decomposition methods. It makes use of and extends the [PETSc framework for numerical computation](http://www.mcs.anl.gov/petsc/). The core of PERMON is formed by the [PermonQP package](permonqp.md) which is able to solve large scale quadratic programming (QP) problems. Among the main applications are contact problems of mechanics and Support Vector Machines (SVM) machine learning. Contact problems can be decomposed by means of TFETI (Total Finite Element Tearing and Interconnecting) non-overlapping domain decomposition method implemented in the [PermonFLLOP package](permonfllop.md). SVM can be solved by [PermonSVM](permonsvm.md). Both PermonFLLOP and PermonSVM make use of PermonQP to solve the resulting QP problems.

**PERMON** is an abbreviation of Parallel, Efficient, Robust, Modular, Object-oriented, Numerical software toolbox developed at [Department of Applied Mathematics](http://am.vsb.cz/en/), [VSB – Technical University of Ostrava](https://www.vsb.cz/en), and [Institute of Geonics of the Czech Academy of Science in Ostrava, Czech Republic](http://www.ugn.cas.cz/). Our city of [Ostrava](https://en.wikipedia.org/wiki/Ostrava) is famous for its coal mining history. "Permon" or "permonik" is also related to coal mining - it is a Czech name for a dwarf helping mineworkers.</p>

## Institutes using PERMON

<center>
[![](assets/logo_argon.jpg)](https://www.anl.gov/)
[![](assets/logo_cvut.png)](https://www.cvut.cz/en/)
[![](assets/logo_losalamos.jpg)](https://www.lanl.gov/)
[![](assets/logo_ornl.jpg)](https://www.ornl.gov/)
</center>
