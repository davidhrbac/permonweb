﻿# PermonFLLOP

The PermonFLLOP package (FLLOP = FETI Light Layer On top of PETSc) is an extension of the [PermonQP](permonqp.md) package, adding domain decomposition methods (DDM) of the FETI type. They combine advantages of iterative and direct solvers, and thanks to that they provide numerical and parallel scalability, and at the same time robustness and accuracy.

![](assets/fllop_166_110.png){ align=right } FETI methods offer efficient utilization of parallel computers up to tens of thousands of processor cores, and allow us this way to shorten the solution time or to compute problems (linear systems or QP) so large that it is not possible to solve them on usual computers. Size of the largest problems successfully solved using PermonFLLOP reaches billions of unknowns, using effectively tens of thousands of processors.

The PermonFLLOP package provides support for assembly of the FETI-specific objects (e.g. so called gluing conditions). The algebraic part of FETI methods is implemented as a special QP transform combining some QP transforms from the PermonQP package.

Current PermonFLLOP applications include mainly problems of structure mechanics (linear elasticity, elasto-plasticity, shape optimization). In combination with PermonIneq, it is able to solve also contact problems.

The PermonQP and PermonFLLOP packages are based on [PETSc](http://www.mcs.anl.gov/petsc/), a software framework for numerical computations. Thanks to that they are usable on all major operating systems and architectures of personal computers as well as supercomputers. The code is written in ANSI C and requires PETSc 3.6 and higher. It is recommended to link PETSc with at least one external parallel direct solver (MUMPS, SuperLU, PaStiX).

## Key features

* [PermonQP](permonqp.md) extension
* FETI DDM implementation for HPC
* FETI-specific objects assembly
* algebraical part of FETI as a special QP transform
* highly scalable solution of linear systems and more general QP problems
* easy-to-use API, 2 styles (PETSc and C)
* file-based interface
* QP problems with no or only equality constraints – using linear system solvers of the [PETSc KSP](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/) package
* QP problems with inequality constraints – using special QP algorithms of the PermonIneq package
* [PETSc](http://www.mcs.anl.gov/petsc/) 3.6 and higher
