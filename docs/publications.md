# Publications

## Selected Papers

### 2016-2017
* Vaclav Hapla, David Horak, Lukáš Pospisil, Martin Cermak, Alena Vasatova, and Radin Sojka. (2016). [Solving Contact Mechanics Problems with PERMON.](http://dx.doi.org/10.1007/978-3-319-40361-8_7) In Lecture Notes in Computer Science (pp. 101–115). Springer International Publishing.
* David Horak, Zdenek Dostal, and Radin Sojka. (2017). [On the Efficient Reconstruction of Displacements in FETI Methods for Contact Problems.](http://advances.utc.sk/index.php/AEEE/article/view/2321) In Advances in Electrical and Electronic Engineering, 15(2).
* Alena Vasatova, Jiri Tomcala, Radim Sojka, Marek Pecha, Jakub Kruzik, David Horak, Václav Hapla, and Martin Cermak. (2017). [Parallel strategies for solving the FETI coarse problem in the PERMON toolbox.](https://dml.cz/handle/10338.dmlcz/703009) In Programs and Algorithms of Numerical Mathematics 18.

### 2018-2020
* Jakub Kruzik, David Horak, Martin Cermak, and Lukas Pospisil. (2020). [Active set expansion strategies in MPRGP algorithm.](https://doi.org/10.1016/j.advengsoft.2020.102895) In Advances in Engineering Software, Volume 149.
*  Radim Blaheta, Michal Beres, Simona Domesova, and David Horak. (2020). [Bayesian inversion for steady flow in fractured porous media with contact on fractures and hydro-mechanical coupling.](https://link.springer.com/article/10.1007/s10596-020-09935-8) Computational Geosciences, Volume 24.
* Jakub Kruzik, David Horak, Vaclav Hapla, and Martin Cermak. (2020). [Comparison of selected FETI coarse space projector implementation strategies.](https://www.sciencedirect.com/science/article/pii/S0167819120300016) In Parallel Computing, Volume 93.
* Serena Crisci, Jakub Kruzik, Marek Pecha, and David Horak. (2020). [Comparison of active-set and gradient projection-based algorithms for box-constrained quadratic programming.](https://doi.org/10.1007/s00500-020-05304-w) In Soft Computing, Volume 24.
* Lukas Pospisil, Martin Cermak, David Horak, and Jakub Kruzik. (2020). [Non-Monotone Projected Gradient Method in Linear Elasticity Contact Problems with Given Friction.](https://doi.org/10.3390/su12208674) In Sustainability 2020 (12).
* Jiri Tomcala, Jan Papuga, David Horak, Vaclav Hapla, Marek Pecha, and Martin Cermak. (2019). [Steps to increase practical applicability of PragTic software.](https://www.sciencedirect.com/science/article/pii/S0965997817309353) In Advances in Engineering Software, Volume 129.
* Radim Sojka, David Horak, Vaclav Hapla, and Martin Cermak. (2018). [The impact of enabling multiple subdomains per MPI process in the TFETI domain decomposition method.](https://www.sciencedirect.com/science/article/pii/S0096300317304927) In Applied Mathematics and Computation, Volume 319.

## Theses

### 2016

* Vaclav Hapla. [Massively Parallel Quadratic Programming Solvers with Applications in Mechanics.](assets/pdf/theses/Hapla_PhD_thesis_2016.pdf)
### 2015
* Lukas Pospisil. [Development of Algorithms for Solving Minimizing Problems with Convex Quadratic Function on Special Convex Sets and Applications.](assets/pdf/theses/Pospisil_PhD_thesis_2015.pdf)
