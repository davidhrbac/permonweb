﻿# PermonSVM

The PermonSVM package provides an implementation of binary classification via soft-margin Support Vector Machines (SVM), designed for use on clusters. PermonSVM implements scalable training procedure based on a linear kernel, taking advantage of an implicit representation of the Gramm matrix, and scalable matrix vector product of [PETSc matrices](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/index.html). The resulting quadratic programming (QP) problem with an implicit Hessian is solved by scalable QP solvers within the [PermonQP](permonqp.md) package, combining QP transforms, an augmented Lagrangian approach (SMALXE), and efficient solvers for box constrained QP.

Our implementation of the SVM training procedure offers efficient utilization of parallel computers up to thousands of processor cores, and allows to substantially shorten the time of SVM training.

The largest problem successfully solved using the PermonSVM was the [benchmark of suspicious URL prediction](https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html#url) with more than 2 million examples and over 3 million features. Reached accuracy was 99.09% computed on 120 cores in 30 seconds.

<center>
![Benchmark of suspicious URL prediction](assets/svm_url_scaling.png)
</center>

Another real-world problem solved using our package is a ground truth learning in material engineering, it is detecting the brittle and ductile fractures on the DWTT specimen surface.

<center>
![Brittle and ductile fractures](assets/ground_truth.png)
</center>

## Key features

* scalable solution of binary classification SVM problems
* implementation of the linear kernel
* L1 and L2 loss function
* parser for SVMLight datasets, HDF5, and PETSc binary file format
* fast, load-balanced hyperparameters-search
* easy-to use PETSc-like API
* dependencies: [PermonQP](permonqp.md) and [PETSc](http://www.mcs.anl.gov/petsc/) 3.6 or higher

## Download

!!! Hint
    Please use this site to send us issues and pull requests.

Public git repository: https://github.com/permon/permonsvm
