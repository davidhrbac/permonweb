﻿# PermonQP

PermonQP is a package providing a base for solution of quadratic programing (QP) problems.

![](assets/qp_chain.png){ align=left }

It includes data structures, transformations, algorithms, and supporting functions for QP. Chain of QP problems Programming interface (API) is carefully designed to be easy-to-use, and at the same time efficient and suitable for HPC. The solution process is from the user’s point of view divided into the following sequence of actions: QP problem specification; performing QP transforms, which reformulate the original problem and create sort of a chain of QP problems where the last one is passed to the solver; automatic or manual choice of an appropriate solver; the solver is called.

PermonQP as a stand-alone package allows solving unconstrained QP problems (i.e. linear systems with a positive semidefinite matrix) or equality constrained ones. In both cases it makes use of the [PETSc KSP](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/) package which includes both direct and iterative solvers, including interfaces to many external solvers. Examples of equality constraints are for instance so called multipoint constraints or the alternative enforcing of Dirichlet boundary conditions without modifying the stiffness matrix.

PermonQP capabilities can be further extended by using PermonIneq (algorithms for solution of inequality constrained problems) and/or PermonFLLOP (FETI domain decomposition methods).<

The PermonQP package extends the [PETSc](http://www.mcs.anl.gov/petsc/) software framework for numerical computations. It is therefore usable on all main operating systems and architectures of personal computers and supercomputers. The code is written in ANSI C and requires PETSc 3.6 or higher. It is recommended to link PETSc with at least one external parallel direct solver (MUMPS, SuperLU, PaStiX).

## Key features

* scalable solution of linear systems and more general QP problems
* easy-to use API, 2 different “skins” (PETSc and C)
* file based interface
* QP transforms
* QP problems with no or equality constraints – making use of the [PETSc KSP](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/) linear system solvers
* equality constrained QP problems – making use of the special QP algorithms in the PermonIneq package
* PermonFLLOP adds domain decomposition methods of the FETI type
* needs [PETSc](http://www.mcs.anl.gov/petsc/) 3.6 and higher

## Download

!!! Hint
    Please use this site to send us issues and pull requests.

Public git repository: https://github.com/permon/permon

